package classes

data class KotlinDog(var name: String, val age: Int)
