package classes;

import java.util.Objects;

public class JavaDog {

    private String name;
    private int age;

    public JavaDog(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        JavaDog javaDog = (JavaDog) o;
        return age == javaDog.age &&
                Objects.equals(name, javaDog.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, age);
    }

    @Override
    public String toString() {
        return "JavaDog{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}
